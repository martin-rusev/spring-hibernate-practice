package springhibernate.practice.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "student_details")
@Getter
@Setter
@NoArgsConstructor
public class StudentDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "hobby")
    private String hobby;

    @Column(name = "favorite_sport")
    private String favoriteSport;

    @OneToOne(mappedBy = "studentDetails")
    private Student student;

    public StudentDetails(String hobby, String favoriteSport) {
        this.hobby = hobby;
        this.favoriteSport = favoriteSport;
    }
}
