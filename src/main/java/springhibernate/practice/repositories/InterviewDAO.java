package springhibernate.practice.repositories;

import springhibernate.practice.entities.Interview;

import java.util.List;

public interface InterviewDAO {

    List<Interview> getInterviews();

    Interview getInterview(int id);

    void save(Interview interview);

    void deleteInterview(int id);
}
