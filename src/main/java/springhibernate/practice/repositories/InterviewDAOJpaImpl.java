package springhibernate.practice.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import springhibernate.practice.entities.Interview;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class InterviewDAOJpaImpl implements InterviewDAO {
    private final EntityManager entityManager;

    @Autowired
    public InterviewDAOJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Interview> getInterviews() {

        Query query = entityManager.createQuery("from Interview", Interview.class);

        List<Interview> interviews = query.getResultList();

        return interviews;
    }

    @Override
    public Interview getInterview(int id) {

        Interview interview = entityManager.find(Interview.class, id);

        return interview;
    }

    @Override
    public void save(Interview interview) {

        Interview dbInterview = entityManager.merge(interview);

        interview.setId(dbInterview.getId());
    }

    @Override
    public void deleteInterview(int id) {

        Query query = entityManager.createQuery("delete from Interview where id=:id");

        query.setParameter("id", id);

        query.executeUpdate();
    }
}
