package springhibernate.practice.repositories;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import springhibernate.practice.entities.Interview;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class InterviewDAOHibernateImpl implements InterviewDAO {
    private final EntityManager entityManager;

    @Autowired
    public InterviewDAOHibernateImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Interview> getInterviews() {
        Session session = entityManager.unwrap(Session.class);

        Query<Interview> query = session.createQuery("from Interview", Interview.class);

        List<Interview> interviews = query.getResultList();

        return interviews;
    }

    @Override
    public Interview getInterview(int id) {

        Session session = entityManager.unwrap(Session.class);

        Interview interview = session.get(Interview.class, id);

        return interview;
    }

    @Override
    public void save(Interview interview) {

        Session session = entityManager.unwrap(Session.class);

        session.saveOrUpdate(interview);
    }

    @Override
    public void deleteInterview(int id) {
        Session currentSession = entityManager.unwrap(Session.class);

        Query theQuery = currentSession.createQuery("delete from Interview where id=:id");

        theQuery.setParameter("id", id);

        theQuery.executeUpdate();
    }
}
