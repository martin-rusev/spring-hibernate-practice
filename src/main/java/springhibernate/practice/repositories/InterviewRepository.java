package springhibernate.practice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import springhibernate.practice.entities.Interview;

public interface InterviewRepository extends JpaRepository<Interview,Integer> {
}
