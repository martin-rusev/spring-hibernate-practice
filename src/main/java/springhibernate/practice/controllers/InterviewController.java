package springhibernate.practice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springhibernate.practice.entities.Interview;
import springhibernate.practice.services.InterviewService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class InterviewController {
    private final InterviewService interviewService;

    @Autowired
    public InterviewController(InterviewService interviewService) {
        this.interviewService = interviewService;
    }

    @GetMapping("/interviews")
    public List<Interview> getInterviews() {
        return interviewService.getInterviews();
    }

    @GetMapping("/interviews/{id}")
    public Interview getInterview(@PathVariable int id) {
        return interviewService.getInterview(id);
    }

    @PostMapping("/interviews")
    public Interview addInterview(@RequestBody Interview interview) {

        interview.setId(0);

        interviewService.save(interview);

        return interview;
    }

    @PutMapping("/interviews")
    public Interview updateInterview(@RequestBody Interview interview) {

        interviewService.save(interview);

        return interview;
    }

    @DeleteMapping("/interviews/{id}")
    public Interview deleteInterview(@PathVariable int id) {

        Interview interview = interviewService.getInterview(id);

        interviewService.deleteInterview(id);

        return interview;
    }

}
