package springhibernate.practice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import springhibernate.practice.entities.Interview;
import springhibernate.practice.repositories.InterviewDAO;
import springhibernate.practice.repositories.InterviewRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class InterviewServiceImpl implements InterviewService{
    private final InterviewRepository interviewRepository;

    @Autowired
    public InterviewServiceImpl(InterviewRepository interviewRepository) {
        this.interviewRepository = interviewRepository;
    }

    @Override
    public List<Interview> getInterviews() {
        return interviewRepository.findAll();
    }

    @Override
    public Interview getInterview(int id) {
        Optional<Interview> result = interviewRepository.findById(id);

        Interview interview = null;

        if (result.isPresent()) {
            interview = result.get();
        }
        else {
            throw new RuntimeException("Did not find interview with id - " + id);
        }

        return interview;
    }

    @Override
    public void save(Interview interview) {
        interviewRepository.save(interview);
    }

    @Override
    public void deleteInterview(int id) {
        interviewRepository.deleteById(id);
    }
}
