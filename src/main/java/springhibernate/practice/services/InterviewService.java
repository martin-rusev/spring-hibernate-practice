package springhibernate.practice.services;

import springhibernate.practice.entities.Interview;

import java.util.List;

public interface InterviewService {
    List<Interview> getInterviews();

   Interview getInterview(int id);

    void save(Interview interview);

    void deleteInterview(int id);
}
