package springhibernate.practice;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springhibernate.practice.entities.Course;
import springhibernate.practice.entities.Student;

import java.util.Set;

public class Demo {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("annotation-applicationContext.xml");

        SessionFactory sessionFactory = (SessionFactory) context.getBean("sessionFactory");

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Student student = session.get(Student.class, 3);

            Set<Course> courseList = student.getCourses();

            for (Course course : courseList) {
                course.getStudents().remove(student);
                session.save(course);
            }

            session.remove(student);

            session.getTransaction().commit();

        }
    }
}
